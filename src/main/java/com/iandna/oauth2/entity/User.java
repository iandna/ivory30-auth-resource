package com.iandna.oauth2.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Builder
@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user")
public class User extends BaseTimeEntity implements UserDetails {
    @Id // pk
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long msrl;      //자동증가 serial
    @Column(nullable = false, unique = true, length = 50)
    private String uid;     // unique_id (email 주소)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(length = 100)
    private String password;    //암호화비밀번호
    @Column(nullable = false, length = 100)
    private String name;        //고객명
    @Column(length = 100)
    private String provider;    //
    @Transient
    private Set<GrantedAuthority> authorities;

    public User(String uid, Set<GrantedAuthority> authorities) {
        this.uid = uid;
        this.authorities = authorities;
    }

    @ElementCollection(fetch = FetchType.EAGER)
    @Builder.Default
    private List<String> roles = new ArrayList<>();     //사용자 ROLE (ROLE_USER 단일사용)

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList("ROLE_USER").stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public String getUsername() {
        return this.uid;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public boolean isEnabled() {
        return true;
    }

    public Optional<String> getProvider() {
        return Optional.ofNullable(provider);
    }

    public void setProvider(String provider) {
        if (provider == null || provider.isEmpty())
            this.provider = null;
        else
            this.provider = provider;
    }
}