package com.iandna.oauth2.controller;

import com.iandna.oauth2.repo.UserJpaRepo;
import com.iandna.oauth2.dto.NewUserDto;
import com.iandna.oauth2.dto.UserResponseDto;
import com.iandna.oauth2.entity.User;
import com.iandna.oauth2.service.UserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/v1")
public class UserController {
    private final Logger LOG = LoggerFactory.getLogger(UserController.class);

    private final UserJpaRepo userJpaRepo;
    private final UserService userService;

    @GetMapping(value = "/users")
    public List<User> findAllUser() {
        return userJpaRepo.findAll();
    }

    @PostMapping(value = "/user")
    public UserResponseDto findUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        String uid = authentication.getPrincipal().toString();
        User entity = userJpaRepo.findByUid(uid)
                .orElseThrow(()-> new IllegalArgumentException("해당 유저가 없습니다."));

        return new UserResponseDto(entity);
    }

    @PostMapping(value = "/sign-up")
    public Long newUser(@RequestBody NewUserDto newUserDto) {
        LOG.info("input ",newUserDto.toString());
        return userService.save(newUserDto);
    }
}
