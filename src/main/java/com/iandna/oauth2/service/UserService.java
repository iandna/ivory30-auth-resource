package com.iandna.oauth2.service;

import com.iandna.oauth2.dto.NewUserDto;
import com.iandna.oauth2.entity.User;
import com.iandna.oauth2.repo.UserJpaRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@RequiredArgsConstructor
@Service
public class UserService {
    private final UserJpaRepo userJpaRepo;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public Long save(NewUserDto newUserDto) {
        return userJpaRepo.save(User.builder()
                        .uid(newUserDto.getEmail())
                        .password(passwordEncoder.encode(newUserDto.getPassword()))
                        .name(newUserDto.getName())
                        .roles(Collections.singletonList("ROLE_USER"))
                        .build()).getMsrl();
    }
}
