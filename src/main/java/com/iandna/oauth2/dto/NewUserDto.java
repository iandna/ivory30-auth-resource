package com.iandna.oauth2.dto;

import com.iandna.oauth2.entity.User;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Collections;

@Getter
@NoArgsConstructor
public class NewUserDto {
    private String name;
    private String email;
    private String phoneNo;
    private String password;

    @Builder
    public NewUserDto(String name, String email, String phoneNo, String password) {
        this.name = name;
        this.email = email;
        this.phoneNo = phoneNo;
        this.password = password;
    }

    @Override
    public String toString() {
        return "NewUserDto{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                '}';
    }

    public User toEntity() {
        return User.builder()
                .uid(email)
                .name(name)
                .roles(Collections.singletonList("ROLE_USER"))
                .build();
    }
}
