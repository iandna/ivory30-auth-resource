package com.iandna.oauth2.dto;

import com.iandna.oauth2.entity.User;

public class UserResponseDto {
    private String id;
    private String name;
    private String email;

    public UserResponseDto(User entity) {
        this.id = String.valueOf(entity.getMsrl());
        this.name = entity.getName();
        this.email = entity.getUid();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "UserVO{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
